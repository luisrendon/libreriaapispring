package co.apilibreria.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import co.apilibreria.model.Encargado;
import co.apilibreria.service.EncargadoService;

@RestController
public class EncargadoController {

	@Autowired
	private EncargadoService encargadoService;

	@GetMapping("/encargados")
	public ResponseEntity<List<Encargado>> getEncargado() {
		List<Encargado> list = encargadoService.list();
		return ResponseEntity.ok().body(list);
	}

	// save encargados
	@PostMapping("/encargado")
	public ResponseEntity<?> save(@RequestBody Encargado encargado) {
		Long id = encargadoService.save(encargado);
		return ResponseEntity.ok().body("Se registró el encargado de id " + id);
	}

	// trae encargado por id
	@GetMapping("/encargado/{id}")
	public ResponseEntity<Encargado> get(@PathVariable("id") Long id) {
		Encargado encargado = encargadoService.get(id);
		return ResponseEntity.ok().body(encargado);
	}

	// Actualiza los datos del encargado
	@PutMapping("/actualizarEncargado/{id}")
	public ResponseEntity<?> update(@RequestBody Encargado encargado, @PathVariable Long id) {
		encargadoService.update(id, encargado);
		return ResponseEntity.ok().body("Encargado actualizado exitosamente");
	}

	// trae encargado por id
	@GetMapping("/encargado/login/{correo}/{contrasena}")
	public ResponseEntity<?> get(@PathVariable("correo") String correo, @PathVariable("contrasena") String contrasena) {
		Encargado encargado = encargadoService.login(correo, contrasena);
		if (encargado != null) {
			return ResponseEntity.ok().body(encargado);
		} else {
			return ResponseEntity.badRequest().body("no se encontró un encargado con los datos suministrados");
		}
	}

}
