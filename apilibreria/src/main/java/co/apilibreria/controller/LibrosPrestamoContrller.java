package co.apilibreria.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import co.apilibreria.model.Encargado;
import co.apilibreria.model.Libro;
import co.apilibreria.service.LibrosPrestamoService;

@RestController
public class LibrosPrestamoContrller {

	@Autowired
	private LibrosPrestamoService librosPrestamoService;

	// Agregar detalles al prestamo.

	// save encargados
	@GetMapping("/prestamos/{idZona}")
	public ResponseEntity<?> getLibrosZona(@PathVariable Long idZona) {
		List<Libro> librosZona = librosPrestamoService.getLibrosPrestadosZona(idZona);
		if (librosZona == null) {
			return ResponseEntity.badRequest().body("no se encontraron prestamos en esta zona");
		}

		return ResponseEntity.ok().body(librosZona);
	}

}
