package co.apilibreria.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import co.apilibreria.model.Encargado;
import co.apilibreria.model.Prestamo;
import co.apilibreria.service.PrestamoService;

@RestController
public class PrestamoController {
	@Autowired
	private PrestamoService prestamoService;

	//Registrar Prestamo
	@PostMapping("/prestamo")
	public ResponseEntity<Prestamo> save(@RequestBody Prestamo prestamo) {
		 Prestamo registrado= prestamoService.registrarPrestamo(prestamo);
		return ResponseEntity.ok().body(registrado);
	}
}
