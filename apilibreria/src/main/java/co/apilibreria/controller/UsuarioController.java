package co.apilibreria.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import co.apilibreria.model.Usuario;
import co.apilibreria.service.UsuarioService;

@RestController
public class UsuarioController {

	@Autowired
	private UsuarioService usuarioService;

	// listar Usuarios por correo
	@GetMapping("/usuarios/{correo}")
	public ResponseEntity<?> get(@PathVariable("correo") String correo) {
		List<Usuario> usuarios = usuarioService.findUsuarios(correo);
		if (usuarios== null) {
			return ResponseEntity.badRequest().body("No hay usuarios registrados con el correo "+correo);
		} else {
			return ResponseEntity.ok().body(usuarios);
		}
	}

}
