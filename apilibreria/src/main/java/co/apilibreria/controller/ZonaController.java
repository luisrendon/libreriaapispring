package co.apilibreria.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.http.ResponseEntity.BodyBuilder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import co.apilibreria.model.Usuario;
import co.apilibreria.model.Zona;
import co.apilibreria.service.ZonaService;

@RestController
public class ZonaController {

	@Autowired
	private ZonaService zonaService;

	// TODO Publish services
	@GetMapping("/zonas")
	public ResponseEntity<?> get() {
		List<Zona> zonas = zonaService.getZonas();
		if (zonas == null) {

			return ((BodyBuilder) ResponseEntity.notFound()).body("imposible conectar con el servidor");
		} else {
			return ResponseEntity.ok().body(zonas);
		}
	}
}
