package co.apilibreria.dao;

import java.util.List;

import co.apilibreria.model.Encargado;

public interface EncargadoDao {

	// Guardar Encargado
	Long save(Encargado encargado);

	// Obtener un encargado por id
	Encargado get(Long id);

	// obtener lista de Encargados
	List<Encargado> list();

	// Actualizar encargado
	void update(Long id, Encargado encargado);

	// Eliminar Encargado
	void delete(Long id);

	// login encargados
	Encargado login(String correo, String contrasena);

}
