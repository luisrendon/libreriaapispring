package co.apilibreria.dao;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import co.apilibreria.model.Encargado;

@Repository
public class EncargadoDaoImpl implements EncargadoDao {

	// obtiene sesión actual para realizar consultas
	@Autowired
	private SessionFactory sessionFactory;

	@Override
	public Long save(Encargado encargado) {
		sessionFactory.getCurrentSession().save(encargado);
		return encargado.getIdEncargado();
	}

	@Override
	public Encargado get(Long id) {

		return sessionFactory.getCurrentSession().get(Encargado.class, id);
	}

	@Override
	public List<Encargado> list() {

		List<Encargado> lista = sessionFactory.getCurrentSession().createQuery("from encargados").list();
		return lista;
	}

	@Override
	public void update(Long id, Encargado encargado) {
		Session session = sessionFactory.getCurrentSession();
		Encargado updateEncargado = session.byId(Encargado.class).load(id);
		updateEncargado = encargado;
		session.flush();
	}

	@Override
	public void delete(Long id) {
		// TODO Auto-generated method stub

	}

	@Override
	public Encargado login(String correo, String contrasena) {
		List<Encargado> encargados = sessionFactory.getCurrentSession()
				.createQuery(
						"From encargados e WHERE e.correo = '" + correo + "' AND e.contrasena= '" + contrasena + "'")
				.list();

		if (encargados.size() == 0) {
			return null;
		} else {
			return encargados.get(0);
		}
	}

}
