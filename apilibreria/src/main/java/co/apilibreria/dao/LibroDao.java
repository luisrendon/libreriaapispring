package co.apilibreria.dao;

import java.util.List;

import co.apilibreria.model.Libro;

public interface LibroDao {

	// consultar todos los libros
	List<Libro> getLibros();

	//Consultar libros prestados
	List<Libro> getLibrosPrestados();

}
