package co.apilibreria.dao;

import java.util.List;

import co.apilibreria.model.Libro;

public interface LibrosPrestamoDao {

	// Consultar los libros prestados en una zona
	List<Libro> getLibrosPrestadosZona(Long idZona);

	
}
