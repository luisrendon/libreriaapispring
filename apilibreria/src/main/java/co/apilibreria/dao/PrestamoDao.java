package co.apilibreria.dao;

import co.apilibreria.model.Prestamo;

public interface PrestamoDao {

	//Calcula el número de días que lleva prestado un libro
	int diasPrestamo();
	
	Prestamo registrarPrestamo(Prestamo prestamo);

}
