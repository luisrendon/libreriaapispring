package co.apilibreria.dao;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import co.apilibreria.model.Prestamo;

@Repository
public class PrestamoDaoImpl implements PrestamoDao {

	// obtiene sesión actual para realizar consultas
	@Autowired
	private SessionFactory sessionFactory;

	@Override
	public int diasPrestamo() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public Prestamo registrarPrestamo(Prestamo prestamo) {
		sessionFactory.getCurrentSession().save(prestamo);
		return prestamo;

	}

}
