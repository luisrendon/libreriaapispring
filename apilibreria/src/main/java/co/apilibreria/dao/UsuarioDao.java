package co.apilibreria.dao;

import java.util.List;

import co.apilibreria.model.Usuario;

public interface UsuarioDao {

	//buscar usuarios por correo
	List<Usuario> findUsuarios(String correo );
	
	
}
