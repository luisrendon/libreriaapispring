package co.apilibreria.dao;

import java.util.List;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import co.apilibreria.model.Encargado;
import co.apilibreria.model.Usuario;

@Repository
public class UsuarioDaoImpl implements UsuarioDao {

	@Autowired
	SessionFactory sessionFactory;

	@Override
	public List<Usuario> findUsuarios(String correo) {
		
		List<Usuario> usuarios = sessionFactory.getCurrentSession()
				.createQuery("From usuarios u WHERE u.correo LIKE '%" + correo + "%'").list();

		if (usuarios.size() == 0) {
			return null;
		} else {
			return usuarios;
		}
	}

}
