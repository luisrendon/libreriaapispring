package co.apilibreria.dao;

import java.util.List;

import co.apilibreria.model.Zona;

public interface ZonaDao {
	//Listar zonas
	List<Zona> getZonas();

}
