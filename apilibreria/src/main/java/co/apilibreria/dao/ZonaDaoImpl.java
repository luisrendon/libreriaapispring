package co.apilibreria.dao;

import java.util.List;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import co.apilibreria.model.Encargado;
import co.apilibreria.model.Zona;

@Repository
public class ZonaDaoImpl implements ZonaDao {

	// obtiene sesión actual para realizar consultas
	@Autowired
	private SessionFactory sessionFactory;

	@Override
	public List<Zona> getZonas() {
		List<Zona> lista = sessionFactory.getCurrentSession().createQuery("from zonas").list();
		if(lista.size()==0) {
			return null;
		}else {
			return lista;			
		}
	}

}
