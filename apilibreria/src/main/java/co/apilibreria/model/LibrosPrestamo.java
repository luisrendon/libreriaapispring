package co.apilibreria.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

@Entity(name = "libros_prestamo")
public class LibrosPrestamo {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "idlibros_prestamo")
	private Long idlibrosPrestamo;

	@ManyToOne
	@JoinColumn(name = "idlibro")
	private Libro libroPrestamo;

	@ManyToOne
	@JoinColumn(name = "idprestamo")
	private Prestamo prestamo;

	public Long getIdlibrosPrestamo() {
		return idlibrosPrestamo;
	}

	public void setIdlibrosPrestamo(Long idlibrosPrestamo) {
		this.idlibrosPrestamo = idlibrosPrestamo;
	}

	public Libro getLibroPrestamo() {
		return libroPrestamo;
	}

	public void setLibroPrestamo(Libro libroPrestamo) {
		this.libroPrestamo = libroPrestamo;
	}

	public Prestamo getPrestamo() {
		return prestamo;
	}

	public void setPrestamo(Prestamo prestamo) {
		this.prestamo = prestamo;
	}

	@Override
	public String toString() {
		return "LibrosPrestamo [idlibrosPrestamo=" + idlibrosPrestamo + ", libroPrestamo=" + libroPrestamo
				+ ", prestamo=" + prestamo + "]";
	}


	


}
