package co.apilibreria.service;

import java.util.List;

import org.springframework.stereotype.Service;

import co.apilibreria.model.Encargado;

@Service
public interface EncargadoService {

	// Guardar Encargado
	Long save(Encargado encargado);

	// Obtener un encargado por id
	Encargado get(Long id);

	// obtener lista de Encargados
	List<Encargado> list();

	// Actualizar encargado
	void update(Long id, Encargado encargado);

	// Eliminar Encargado
	void delete(Long id);
	
	Encargado login(String correo, String contrasena);

}
