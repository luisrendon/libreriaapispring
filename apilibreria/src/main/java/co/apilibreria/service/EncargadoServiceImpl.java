package co.apilibreria.service;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import co.apilibreria.dao.EncargadoDao;
import co.apilibreria.model.Encargado;

@Service
@Transactional(readOnly = true)
public class EncargadoServiceImpl implements EncargadoService {

	@Autowired
	private EncargadoDao encargadoDao;

	@Override
	@Transactional
	public Long save(Encargado encargado) {
		return encargadoDao.save(encargado);
	}

	@Override
	@Transactional
	public Encargado get(Long id) {
		return encargadoDao.get(id);
	}

	@Override
	@Transactional
	public List<Encargado> list() {
		return encargadoDao.list();
	}

	@Override
	@Transactional
	public void update(Long id, Encargado encargado) {
		encargadoDao.update(id, encargado);
	}

	@Override
	public void delete(Long id) {
		// TODO Auto-generated method stub

	}

	@Override
	@Transactional
	public Encargado login(String correo, String contrasena) {

		return encargadoDao.login(correo, contrasena);
	}

}
