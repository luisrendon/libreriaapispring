package co.apilibreria.service;

import java.util.List;

import org.springframework.stereotype.Service;

import co.apilibreria.model.Libro;

@Service
public interface LibroService {
	
	// consultar todos los libros
	List<Libro> getLibros();

	// Consultar libros prestados
	List<Libro> getLibrosPrestados();
}
