package co.apilibreria.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import co.apilibreria.dao.LibroDao;
import co.apilibreria.model.Libro;

@Service
@Transactional(readOnly = true)
public class LibroServiceImpl implements LibroService {

	@Autowired
	private LibroDao LibroDao;

	@Override
	@Transactional
	public List<Libro> getLibros() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	@Transactional
	public List<Libro> getLibrosPrestados() {
		// TODO Auto-generated method stub
		return null;
	}

}
