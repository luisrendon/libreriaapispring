package co.apilibreria.service;

import java.util.List;

import org.springframework.stereotype.Service;

import co.apilibreria.model.Libro;

@Service
public interface LibrosPrestamoService {

	// Consultar los libros prestados en una zona
	List<Libro> getLibrosPrestadosZona(Long idZona);

	
	
}
