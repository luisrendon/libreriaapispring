package co.apilibreria.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import co.apilibreria.dao.LibrosPrestamoDao;
import co.apilibreria.model.Libro;

@Service
@Transactional(readOnly = true)
public class LibrosPrestamoServiceImpl implements LibrosPrestamoService {

	@Autowired
	private LibrosPrestamoDao librosPrestamoDao;

	@Override
	@Transactional
	public List<Libro> getLibrosPrestadosZona(Long idZona) {
		return librosPrestamoDao.getLibrosPrestadosZona(idZona);
	}

}
