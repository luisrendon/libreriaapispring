package co.apilibreria.service;

import org.springframework.stereotype.Service;

import co.apilibreria.model.Prestamo;

@Service
public interface PrestamoService {

	// Calcula el número de días que lleva prestado un libro
	int diasPrestamo();

	Prestamo registrarPrestamo(Prestamo prestamo);

	
}
