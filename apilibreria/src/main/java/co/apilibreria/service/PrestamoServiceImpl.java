package co.apilibreria.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import co.apilibreria.dao.PrestamoDao;
import co.apilibreria.model.Prestamo;

@Service
@Transactional(readOnly = true)
public class PrestamoServiceImpl implements PrestamoService {

	@Autowired
	private PrestamoDao prestamoDao;

	@Override
	@Transactional
	public int diasPrestamo() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	@Transactional
	public Prestamo registrarPrestamo(Prestamo prestamo) {
		
		return prestamoDao.registrarPrestamo(prestamo);
	}

}
