package co.apilibreria.service;

import java.util.List;

import org.springframework.stereotype.Service;

import co.apilibreria.model.Usuario;

@Service
public interface UsuarioService {
	// buscar usuarios por correo
	List<Usuario> findUsuarios(String correo);
}
