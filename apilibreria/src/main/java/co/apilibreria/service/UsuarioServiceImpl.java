package co.apilibreria.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import co.apilibreria.dao.EncargadoDao;
import co.apilibreria.dao.UsuarioDao;
import co.apilibreria.model.Usuario;

@Service
@Transactional(readOnly = true)
public class UsuarioServiceImpl implements UsuarioService {

	
	@Autowired
	private UsuarioDao usuarioDao;
	
	
	@Override
	@Transactional
	public List<Usuario> findUsuarios(String correo) {
	
		return usuarioDao.findUsuarios(correo);
	}

	
	
}
