package co.apilibreria.service;

import java.util.List;

import org.springframework.stereotype.Service;

import co.apilibreria.model.Zona;

@Service
public interface ZonaService {
	// Listar zonas
	List<Zona> getZonas();
}
