package co.apilibreria.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import co.apilibreria.dao.ZonaDao;
import co.apilibreria.model.Zona;

@Service
@Transactional(readOnly = true)
public class ZonaServiceImpl implements ZonaService {

	@Autowired
	private ZonaDao zonaDao;

	@Override
	@Transactional
	public List<Zona> getZonas() {
		return zonaDao.getZonas();
	}

}
